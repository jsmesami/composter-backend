# Composter Backend

The backend part of automated poster generation system for local libraries in Central Bohemian Region.

## Documentation

API is documented [here](docs/API.apib).
Poster specification format is described [here](docs/SPEC.md).

## License

Copyright © 2019 Ondřej Nejedlý

Distributed under the Eclipse Public License either version 1.0 or
(at your option) any later version.
