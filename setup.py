#!/usr/bin/env python
import re
from setuptools import setup, find_packages


def read_requirements(req_file):
    return [line for line in re.sub(r'\s*#.*\n', '\n', req_file.read()).splitlines() if line]


with open('requirements.txt') as f:
    REQUIREMENTS = read_requirements(f)

with open('README.md') as f:
    LONG_DESCRIPTION = f.read()

setup(
    name='composter-backend',
    version='1.0.3',
    description='The backend part of automated poster generation system for local libraries in Central Bohemian Region',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Ondřej Nejedlý',
    author_email='jsmesami@gmail.com',
    url='https://gitlab.com/jsmesami/composter-backend',
    license='EPL',
    python_requires='>=3.11',
    install_requires=REQUIREMENTS,
    tests_require=REQUIREMENTS,
    packages=find_packages(),
    scripts=['src/manage.py'],
    test_suite='tests',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: EPL-1.0 License',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: Python :: 3.12',
    ],
)
