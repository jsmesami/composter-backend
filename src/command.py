"""A wrapper for Django management commands for easier Lambda scheduling."""
import sys


class Runner:
    def __getattr__(self, attr):
        from django.core.management import call_command
        return lambda: call_command(attr)


sys.modules[__name__] = Runner()
