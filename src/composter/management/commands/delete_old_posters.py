from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.core.management import BaseCommand
from django.utils import timezone

from composter.models.poster import Poster


class Command(BaseCommand):

    help = "Delete posters older than {} months.".format(settings.DELETE_POSTERS_AFTER_MONTHS)

    def handle(self, *args, **options):
        old = timezone.now() - relativedelta(months=settings.DELETE_POSTERS_AFTER_MONTHS)
        enabled = Poster.objects.enabled()
        disabled = enabled.filter(modified__lt=old).update(disabled=True)
        remaining = enabled.count()
        print(f"Deleted {disabled} posters, keeping {remaining} posters.")
