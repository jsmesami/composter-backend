import itertools
import re

import boto3
import botocore
from django.conf import settings
from django.core.management import BaseCommand

from composter.models.poster import Poster
from composter.models.image import PosterImage


class Command(BaseCommand):

    help = "Delete documents with no DB references"

    def replace_special(self, s):
        s = re.sub("'", "&apos;", s)
        s = re.sub('"', "&quot;", s)
        s = re.sub('&', "&amp;", s)
        s = re.sub('<', "&lt;", s)
        s = re.sub('>', "&gt;", s)
        s = re.sub('\r', "&#13;", s)
        s = re.sub('\n', "&#10;", s)
        return s

    def handle(self, *args, **options):
        S3 = boto3.resource('s3')
        bucket = S3.Bucket(settings.AWS_STORAGE_BUCKET_NAME)

        referenced_docs = itertools.chain.from_iterable(
            o.values() for o in Poster.objects.all().values('thumb', 'print_pdf', 'print_jpg').iterator()
        )
        referenced_images = itertools.chain.from_iterable(
            PosterImage.objects.all().values_list('file', flat=True).iterator()
        )
        referenced_objects = {f'media/{o}' for o in itertools.chain(referenced_docs, referenced_images)}
        bucket_objects = {o.key for o in bucket.objects.filter(Prefix='media/posters/')}
        objects_to_delete = [self.replace_special(o) for o in (bucket_objects - referenced_objects)]

        if len(objects_to_delete):
            client = S3.meta.client
            # TODO: refactor to bulk delete (resolve "invalid XML" errors)
            for o in objects_to_delete:
                try:
                    client.delete_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=o)
                except botocore.exceptions.ClientError as e:
                    print(e)
