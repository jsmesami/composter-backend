import os
import boto3
from botocore.errorfactory import ClientError
from django.conf import settings
from django.db import models

from composter.models import EnabledQuerySet

S3 = boto3.client('s3')


class Bureau(models.Model):

    name = models.CharField(max_length=255)
    abbrev = models.CharField(max_length=8)
    address = models.CharField(max_length=255)
    disabled = models.BooleanField(default=False)

    objects = models.Manager.from_queryset(EnabledQuerySet)()

    LOGO_URL_PREFIX = 'logos'

    def custom_logo_exists(self, custom_logo_file):
        if settings.DEBUG:
            path = os.path.join(settings.STATIC_ROOT, self.LOGO_URL_PREFIX, custom_logo_file)
            return os.path.isfile(path)
        else:
            try:
                path = f'static/{self.LOGO_URL_PREFIX}/{custom_logo_file}'
                S3.head_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=path)
            except ClientError:
                return False
            else:
                return True

    @property
    def logo(self):
        custom_logo_file = '{}.jpg'.format(self.abbrev)
        custom_logo_url = os.path.join(settings.STATIC_URL, self.LOGO_URL_PREFIX, custom_logo_file)
        default_logo_url = os.path.join(settings.STATIC_URL, self.LOGO_URL_PREFIX, 'default.jpg')
        return custom_logo_url if self.custom_logo_exists(custom_logo_file) else default_logo_url

    class Meta:
        ordering = 'name',
