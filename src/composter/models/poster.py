import os
from functools import partial

from unidecode import unidecode

from django.conf import settings
from django.core.files.base import ContentFile
from django.db import models
from django.utils import timezone
from django.utils.text import slugify

from composter.models import EnabledQuerySet
from composter.models.bureau import Bureau
from composter.models.image import PosterImage
from composter.models.posterspec import PosterSpec
from composter.renderer import Renderer
from utils import urlsafe_hash
from utils.functional import deepmerge
from utils.models import TimeStampedModel


class Poster(TimeStampedModel):

    bureau = models.ForeignKey(Bureau, on_delete=models.CASCADE, related_name='posters')
    spec = models.ForeignKey(PosterSpec, on_delete=models.CASCADE, related_name='posters')
    saved_fields = models.JSONField(editable=False)  # Fields from spec with saved values
    disabled = models.BooleanField(default=False)

    objects = models.Manager.from_queryset(EnabledQuerySet)()

    def _upload_to(self, filename, suffix):
        _, extension = os.path.splitext(filename)

        return 'posters/{filename}{extension}'.format(
            filename='{id:05d}_{bureau.abbrev}_{title}-{hash}_{created}_{suffix}'.format(
                id=self.id,
                bureau=self.bureau,
                title=slugify(unidecode(self.title)),
                hash=urlsafe_hash(self.saved_fields, timezone.now()).rstrip('='),
                created='{y}{m:02d}{d:02d}'.format(
                    d=self.created.day,
                    m=self.created.month,
                    y=str(self.created.year)[-2:],
                ),
                suffix=suffix,
            ),
            extension=extension,
        )

    thumb = models.ImageField(
        max_length=255,
        upload_to=partial(_upload_to, suffix='THUMB'),
        editable=False
    )
    print_pdf = models.FileField(
        max_length=255,
        upload_to=partial(_upload_to, suffix='PRINT'),
        editable=False
    )
    print_jpg = models.FileField(
        max_length=255,
        upload_to=partial(_upload_to, suffix='PRINT'),
        editable=False,
        blank=True
    )

    @property
    def title(self):
        """
        If fields contain a title, return it, else render something meaningful.
        """
        return (self.saved_fields.get('title', {}).get('text') or
                "Poster {self.id} ({self.spec.name})".format(self=self))

    def save(self, **kwargs):
        # Fields being saved need to be populated with data from spec (not present in request).
        populated_fields = deepmerge(
            self.saved_fields,
            {k: v for (k, v) in self.spec.editable_fields.items() if k in self.saved_fields},
        )

        self.saved_fields = PosterImage.save_images_from_fields(populated_fields)

        # Save to get the poster ID
        super().save(**kwargs)

        renderer = Renderer(self.spec, self.saved_fields)
        pdf = renderer.render_pdf()
        self.print_pdf = ContentFile(pdf, name='dummy.pdf')

        jpg = renderer.render_jpg(pdf, **settings.RENDERER['jpg_print_params'])
        self.print_jpg = ContentFile(jpg, name='dummy.jpeg')

        thumb = renderer.render_jpg(pdf, **settings.RENDERER['jpg_thumb_params'])
        self.thumb = ContentFile(thumb, name='dummy.jpeg')

        super().save(force_update=True)

    class Meta:
        ordering = '-modified',
