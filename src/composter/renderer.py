import io
import math
import re
import string

from django.conf import settings

from PIL import Image
from pdf2image import convert_from_bytes

from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import mm
from reportlab.lib.utils import ImageReader
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus.frames import Frame
from reportlab.platypus.paragraph import Paragraph
from storages.backends.s3boto3 import S3Boto3Storage
from urllib.request import urlopen

from composter.models.image import PosterImage, SpecImage
from utils.functional import replace_chars

__all__ = 'Renderer',


class TextFrame:
    _PREPOS_RE = re.compile(r'(?<= |\u00a0)([kosuvzia]) ', re.M | re.I | re.U)

    def __init__(self, **kwargs):
        self.params = kwargs
        self.style = ParagraphStyle(
            name='Normal',
            fontName=settings.RENDERER['default_font_name'],
            fontSize=settings.RENDERER['default_font_size'],
            textColor=settings.RENDERER['default_text_color'],
            leading=settings.RENDERER['default_font_size'] * 1.2
        )

    def draw(self, canvas):
        x = self.params['x'] * mm
        y = self.params['y'] * mm
        w = self.params.get('w', 0) * mm
        h = self.params.get('h', 0) * mm
        align = self.params.get('align', 'left')
        color = self.params.get('color', settings.RENDERER['default_text_color'])
        font_size = self.params.get('font_size', settings.RENDERER['default_font_size'])
        text = replace_chars(self.params['text'], string.whitespace, ' ')
        text = re.sub(r'\s+', ' ', text)
        text = self.change_case(text, self.params.get('case', 'initial'))

        canvas.setFont(settings.RENDERER['default_font_name'], font_size)
        canvas.setFillColor(color)

        if w and h:
            self.draw_frame(canvas, x, y, w, h, self._PREPOS_RE.sub('\\g<1>\u00a0', text))  # noqa pylint: disable=W605
        else:
            self.draw_string(canvas, x, y, align, text)

    def draw_string(self, canvas, x, y, alignment, text):
        method = {
            'left': canvas.drawString,
            'center': canvas.drawCentredString,
            'right': canvas.drawRightString,
        }

        method[alignment](x, y, text)

    def draw_frame(self, canvas, x, y, w, h, text):
        frame = Frame(x, y, w, h, leftPadding=0, bottomPadding=0, rightPadding=0, topPadding=0)
        frame.add(Paragraph(text, self.style), canvas)

    def change_case(self, text, case):
        method = {
            'upper': lambda t: t.upper(),
            'lower': lambda t: t.lower(),
        }

        return text if case == 'initial' else method[case](text)


class ImageFrame:

    def __init__(self, image, **kwargs):
        self.params = kwargs
        self.image = image

    def draw(self, canvas):
        x = self.params['x'] * mm
        y = self.params['y'] * mm
        w = self.params.get('w', 0) * mm
        h = self.params.get('h', 0) * mm
        scaling_method = self.params.get('scale')
        alignment = self.params.get('align')

        img = self._fetch(self.image.file)
        img = self._flatten(img)

        if w and h:
            scaled_img = self._scale(img, scaling_method, w, h)
            canvas.drawImage(ImageReader(scaled_img), *self._align(scaled_img, alignment, x, y, w, h))
        else:
            canvas.drawImage(ImageReader(img), x, y)

    def _fetch(self, image_field):
        if isinstance(image_field.storage, S3Boto3Storage):
            return Image.open(urlopen(image_field.url))

        return Image.open(image_field.path)

    @classmethod
    def _scale(cls, img, method, w, h):
        """
        Scale an image to the aspect ratio specified, leave untouched if the original ratio almost equals.
        """
        orig_w, orig_h = img.size
        if math.isclose(orig_w / orig_h, w / h, abs_tol=0.06):
            return img

        if method == 'crop':
            return cls._crop(img, w, h)

        return img

    @classmethod
    def _crop(cls, img, w, h):
        i_w, i_h = img.size
        crop_w, crop_h = cls.fit_rectangle_into_container(w, h, i_w, i_h)
        crop_x = (i_w - crop_w) / 2
        crop_y = (i_h - crop_h) / 2

        return img.crop((crop_x, crop_y, crop_x + crop_w, crop_y + crop_h))

    @classmethod
    def _flatten(cls, img):
        if img.mode in ('RGBA', 'LA') or (img.mode == 'P' and 'transparency' in img.info):
            bg_img = Image.new('RGBA', img.size, (255, 255, 255))
            img = img.convert('RGBA')
            img = Image.alpha_composite(bg_img, img).convert("RGB")

        return img

    @classmethod
    def _align(cls, img, alignment, x, y, w, h):
        i_w, i_h = img.size
        fit_w, fit_h = cls.fit_rectangle_into_container(i_w, i_h, w, h)

        if alignment == 'right':
            return x + w - fit_w, y, fit_w, fit_h
        elif alignment == 'center':
            return x + (w - fit_w) / 2, y, fit_w, fit_h

        return x, y, fit_w, fit_h

    @staticmethod
    def fit_rectangle_into_container(rect_w, rect_h, cont_w, cont_h):
        cont_ratio = cont_w / cont_h
        rect_ratio = rect_w / rect_h

        if rect_ratio >= cont_ratio:
            scale_ratio = cont_w / rect_w
        else:
            scale_ratio = cont_h / rect_h

        return rect_w * scale_ratio, rect_h * scale_ratio


class Renderer:

    def __init__(self, spec, saved_fields):
        self.page_size = spec.w * mm, spec.h * mm
        self.elements = []

        def get_element(field_name, field_params, images_lookup):
            if field_params['type'] == 'text':
                return TextFrame(**spec.frames[field_name], **field_params)
            else:
                return ImageFrame(images_lookup[field_params['id']], **spec.frames[field_name], **field_params)

        spec_image_ids = {f['id'] for f in spec.static_fields.values() if f['type'] == 'image'}
        spec_images_lookup = {i.pk: i for i in SpecImage.objects.filter(id__in=spec_image_ids)}

        for (name, params) in spec.static_fields.items():
            self.elements.append(get_element(name, params, spec_images_lookup))

        # Filter out fields that are not filled
        poster_fields = {k: v for (k, v) in saved_fields.items() if v.get('id') or v.get('text')}
        poster_image_ids = {f['id'] for f in poster_fields.values() if f['type'] == 'image'}
        poster_images_lookup = {i.pk: i for i in PosterImage.objects.filter(id__in=poster_image_ids)}

        for (name, params) in poster_fields.items():
            self.elements.append(get_element(name, params, poster_images_lookup))

    def render_pdf(self):
        default_font = settings.RENDERER.get('default_font_file')
        if default_font:
            pdfmetrics.registerFont(TTFont(settings.RENDERER['default_font_name'], default_font))

        with io.BytesIO() as buffer:
            c = canvas.Canvas(buffer)

            c.setPageSize(self.page_size)

            for el in self.elements:
                el.draw(c)

            c.showPage()
            c.save()

            return buffer.getvalue()

    def render_jpg(self, pdf, dpi, quality):
        img = convert_from_bytes(pdf, dpi=dpi)[0]

        with io.BytesIO() as buffer:
            img.save(buffer, format='jpeg', quality=quality)

            return buffer.getvalue()
