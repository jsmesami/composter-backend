from django.conf import settings
from django.urls import path, include
from django.conf.urls.static import static

from composter.api import views
from composter.api.routers import router
from composter.api.views import poster_clone


urlpatterns = [
    # We are using custom APIRootView instead of DefaultRouter's root view,
    # because we don't want domain in endpoint urls.
    path('', views.APIRootView.as_view(), name='api-root'),
    path('', include(router.urls)),
    path('poster/clone/<pk>/', poster_clone),
] + (
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) +
    static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
)
