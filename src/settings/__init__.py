from .base import *  # NOQA: F403

if env('ENVIRONMENT') == 'production':  # NOQA: F405
    from .aws import *  # NOQA: F403, F401
