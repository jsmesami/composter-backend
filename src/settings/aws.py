from utils.env import env

DEFAULT_FILE_STORAGE = 'utils.aws.MediaStorage'

STATICFILES_STORAGE = DEFAULT_FILE_STORAGE

AWS_STORAGE_BUCKET_NAME = env('S3_BUCKET_NAME')

AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'no-cache',
}

AWS_S3_REGION_NAME = env('S3_REGION')

AWS_QUERYSTRING_AUTH = False

MEDIA_URL = f'//s3.{AWS_S3_REGION_NAME}.amazonaws.com/{AWS_STORAGE_BUCKET_NAME}/media/'

MEDIA_ROOT = MEDIA_URL

STATIC_URL = f'//s3.{AWS_S3_REGION_NAME}.amazonaws.com/{AWS_STORAGE_BUCKET_NAME}/static/'

STATIC_ROOT = STATIC_URL
