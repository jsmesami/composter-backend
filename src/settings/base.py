from pathlib import Path

import composter
from utils.env import env


BASE_DIR = Path(composter.__file__).parent.parent

DEBUG = env('ENVIRONMENT') == 'development'

PROJECT_DOMAIN = env('PROJECT_DOMAIN')

SECRET_KEY = env('DJANGO_SECRET_KEY')

ALLOWED_HOSTS = [PROJECT_DOMAIN]

ADMINS = MANAGERS = (
    ('Ondřej Nejedlý', 'jsmesami@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('DEFAULT_DB_NAME', 'composter'),
        'USER': env('DEFAULT_DB_USER', 'composter'),
        'PASSWORD': env('DEFAULT_DB_PASSWORD', ''),
        'HOST': env('DEFAULT_DB_HOST', ''),
        'PORT': env('DEFAULT_DB_PORT', ''),
    },
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

USE_I18N = True
USE_TZ = False

TIME_ZONE = 'Europe/Prague'

LANGUAGE_CODE = 'cs'

MEDIA_ROOT = env('DJANGO_MEDIA_ROOT', BASE_DIR / 'media')

MEDIA_URL = env('DJANGO_MEDIA_URL', '/media/')

STATIC_ROOT = env('DJANGO_STATIC_ROOT', BASE_DIR / 'static')

STATIC_URL = env('DJANGO_STATIC_URL', '/static/')

ROOT_URLCONF = 'composter.urls'

WSGI_APPLICATION = 'composter.wsgi.application'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.postgres',
    'corsheaders',
    'django.contrib.staticfiles',
    'rest_framework',
    'storages',
    'composter',
)

MIDDLEWARE = (
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
)

CORS_ALLOWED_ORIGINS = (
    env('CLIENT_URL'),
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(process)d %(message)s',
        },
    },
    'filters': {
        'debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'syslog': {
            'level': 'INFO',
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'verbose',
            'address': '/dev/log',
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'composter': {
            'level': 'DEBUG',
            'handlers': ['syslog', 'console'],
        },
    },
}

EMAIL_HOST = env('EMAIL_HOST')

EMAIL_PORT = env('EMAIL_PORT')

EMAIL_USE_TLS = env('ENVIRONMENT') == 'production'

EMAIL_HOST_USER = env('EMAIL_HOST_USER', '')

EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD', '')

SERVER_EMAIL = 'jsmesami@gmail.com'

DEFAULT_FROM_EMAIL = SERVER_EMAIL

EMAIL_SUBJECT_PREFIX = '[Composter] '

REST_FRAMEWORK = {
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_RENDERER_CLASSES': ['rest_framework.renderers.JSONRenderer'],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
}

UPLOADED_FILE_MAX_SIZE = 3 * 1024 * 1024

SUPPORTED_IMAGE_EXTENSIONS = '.jpeg', '.jpg', '.png'

DELETE_POSTERS_AFTER_MONTHS = 18

RENDERER = {
    'default_font_name': 'LiberationSans',
    'default_font_file': BASE_DIR / 'fonts' / 'LiberationSans-Regular.ttf',
    'default_font_size': 16,
    'default_text_color': '#000000',
    'jpg_print_params': {'dpi': '200', 'quality': 90},
    'jpg_thumb_params': {'dpi': '40', 'quality': 40}
}
