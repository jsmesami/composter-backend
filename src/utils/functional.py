import functools


def deepmerge(source: dict, destination: dict) -> dict:
    """
    Recursively merge source dictionary into destination.
    Use `reduce(deepmerge, [d1, d2, d3])` to merge multiple dictionaries
    """
    for key, value in source.items():
        if isinstance(value, dict):
            node = destination.setdefault(key, {})
            deepmerge(value, node)
        else:
            destination[key] = value

    return destination


def replace_chars(string_: str, chars: str, replacement: str) -> str:
    """
    Within a string, replace occurrences of each char of chars with replacement string
    """
    return functools.reduce(lambda acc, ch: acc.replace(ch, replacement), chars, string_)


def translate_chars(string_: str, table: dict) -> str:
    """
    Within a string, translate occurrences of table keys to table values
    """
    return functools.reduce(lambda acc, ch: acc.replace(ch, table[ch]), table.keys(), string_)
